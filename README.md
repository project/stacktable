CONTENTS OF THIS FILE
---------------------

* Introduction
* Installation
* Maintainers


INTRODUCTION
------------
This module provides integration for the Stacktable.js plugin for creating
responsive tables. The purpose of Stacktable.js is to give you an easy way
of converting wide tables to a format that will work better on small screens.
It creates a copy of the table that is converted into a 2-column key/value form


INSTALLATION
------------

Install via Composer (recommended)

If you use Composer to manage dependencies, edit composer.json as follows.

* Run `composer require --prefer-dist composer/installers` to ensure you have
  the composer/installers package. This facilitates installation into directories
  other than vendor using Composer.

* In composer.json, make sure the "installer-paths" section in "extras" has an
  entry for `type:drupal-library`. For example:

```json
{
  "libraries/{$name}": ["type:drupal-library"]
}
```

* Add the following to the "repositories" section of composer.json:

```json
{
  "type": "package",
  "package": {
    "name": "johnpolacek/stacktablejs",
    "version": "1.0.3",
    "type": "drupal-library",
    "dist": {
      "url": "https://github.com/johnpolacek/stacktable.js/archive/refs/tags/1.0.3.zip",
      "type": "zip"
    }
  }
}
```

* Run `composer require 'johnpolacek/stacktablejs:1.0.3'` to download the plugin.

* Run `composer require 'drupal/stacktable:^1.0.0'` to download the
  Stacktable module, and enable it [as per usual](
  https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules).

* At admin/config/system/stacktable specify the desired form id and class table.

Install Manually

* Download the [stacktable.js](https://github.com/johnpolacek/stacktable.js/tags)
  plugin plugin.

* Extract and place the plugin contents in the following directory:
  `/libraries/stacktablejs/`.

* Install the Stacktable module [as per usual](
  https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules).

* At admin/config/user-interface/stacktable specify the desired form id and class table.


MAINTAINERS
-----------
Current maintainers:
* rutel95 - https://www.drupal.org/u/rutel95
* hkirsman - https://www.drupal.org/u/hkirsman
