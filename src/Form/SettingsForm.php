<?php

namespace Drupal\stacktable\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form.
 *
 * @package Drupal\stacktable\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['stacktable.settings'];
  }

  /**
   * Administrative pages of Stacktable module.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return array
   *   Renderable form array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('stacktable.settings');

    $form['query'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Selector'),
      '#description' => $this->t('A list of HTML selectors. One selector per line (for example: <strong>table</strong> to apply to all tables on page or <strong>table.my_table</strong> to apply to tables with class my_table.)'),
      '#default_value' => $config->get('query'),
    ];

    $form['responsive_method'] = [
      '#type' => 'select',
      '#title' => $this->t('Responsive method'),
      '#required' => TRUE,
      '#options' => [
        'default' => 'Default',
        'on_demand' => 'On demand (experimental)',
      ],
      '#default_value' => $config->get('responsive_method'),
    ];

    $form['responsive_method']['#description'] = $this->t('<strong>Default</strong> - uses stacktable.js default styles to control when to switch to stacktable table via media query.');
    $form['responsive_method']['#description'] .= '<br>';
    $form['responsive_method']['#description'] .= $this->t("<strong>On demand</strong> - switches to stacktable only when table does not fit in it's container. This is not part of the stacktable.js library itself, so it's marked experimental.");
    $form['responsive_method']['#description'] .= ' ' . $this->t("Part of this is also feature to <strong>open the table in modal</strong> by manually adding stacktable__open-table-in-modal class to table.");

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('stacktable.settings')
      ->set('query', $form_state->getValue('query'))
      ->set('responsive_method', $form_state->getValue('responsive_method'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'stacktable_settings';
  }

}
