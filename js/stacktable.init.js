/**
 * @file
 * stacktable.init.js
 */

(function ($, Drupal, drupalSettings, once) {
  const tableCache = [];

  function enableOnlyWhenTableDoesNotFit($table) {
    const $tableContainer = $table.parent();

    if ($table.is(':visible')) {
      if ($table.width() > $tableContainer.width()) {
        // Save the initial table size.
        // It must be smallest it can get at this stage.
        $table.data('stacktable-original-table-width', $table.width());

        $table.hide();
        $table.prev().css('display', 'table');
      }
    } else {
      const originalTableWidth = $table.data('stacktable-original-table-width');
      if (originalTableWidth <= $tableContainer.width()) {
        $table.css('display', 'table');
        $table.prev().hide();
      }
    }
  }

  /**
   * Open table in Drupal modal.
   *
   * Mostly needed for big tables.
   */
  const openTableInModal = function ($table) {
    let $tableClone = $table.clone();

    // Remove the placeholders styles when inside modal.
    $tableClone.removeClass('stacktable__open-table-in-modal');

    // Make the table responsive (scrollable).
    $tableClone.wrap('<div class="stacktable__table-responsive" />');

    // Add dummy link so modal would not focus on some link inside content and
    // scroll away from top.
    $tableClone.before('<a href="#" style="font-size: 0;"></a>');

    // Get the parent .stacktable__table-responsive element for modal.
    $tableClone = $tableClone.parent();

    const modal = Drupal.dialog($tableClone, {
      title: false,
      modal: true,
      width: '95%',
      height: $(window).height() - 100,
      open(event, ui) {
        $('.ui-widget-overlay').css('z-index', 1000);
        $('.ui-dialog').css('z-index', 1001);

        $('body').css('overflow', 'hidden');
        $('.body-inner').css('overflow', 'hidden');
      },
      close(event, ui) {
        $('body').css('overflow', '');
        $('.body-inner').css('overflow', '');
      }
    });
    modal.showModal();
  };

  /**
   * On resize decide if table should be switched to normal or stacktable.
   */
  $(window).resize(function () {
    clearTimeout(window.resizedFinished);
    window.resizedFinished = setTimeout(function () {
      tableCache.forEach(function ($table) {
        enableOnlyWhenTableDoesNotFit($table);
      });
    }, 150);
  });

  Drupal.behaviors.stacktable = {
    attach: function attach(context) {
      const $tables = $(
        once('stacktable', drupalSettings.stacktable.query, context)
      );

      const enableWhenTableDoesNotFit =
        drupalSettings.stacktable.responsive_method;

      $tables.each(function () {
        const $table = $(this);

        // Make the table stacktable.
        $table.stacktable();

        // "On demand" experimental feature.
        // Make also all tables stacktables, but hide them by default.
        // It'll start toggling between the generated responsive table
        // and original in enableOnlyWhenTableDoesNotFit() via
        // $(window).resize() callback.
        // If the table is larger than it's container then it'll switch to
        // stacktable table.
        if (enableWhenTableDoesNotFit === 'on_demand') {
          // Sub-feature to allow opening tables in modal
          if ($table.hasClass('stacktable__open-table-in-modal')) {
            $table.prev().removeClass('stacktable__open-table-in-modal');
            $table.click(function () {
              openTableInModal($table);
            });
          }

          // Hide the stacktable by default.
          $table.prev().css('display', 'none');

          // Check if the original table fits, and it not then show the
          // stacktable table.
          enableOnlyWhenTableDoesNotFit($table);

          // Save the table for later reference from
          // enableOnlyWhenTableDoesNotFit().
          tableCache[tableCache.length] = $table;
        }
      });
    }
  };
})(jQuery, Drupal, drupalSettings, once);
